<?php
  date_default_timezone_set('America/Santiago');
  ini_set('display_errors', 'Off');
  set_time_limit(3600);
  // require('consultas.php');
  // require('simple_html_dom.php');

  // $cookie = "/home/rriveros/public_html/pjud/pjudcookieTT.txt";
  $cookie = "cookieTT.txt";

  $url = "https://gestion.ecertchile.cl/Default.aspx";

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0");
  curl_setopt($ch, CURLOPT_HEADER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
  curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
  curl_setopt($ch, CURLOPT_POST, false);

  $respuesta = curl_exec($ch);

  curl_close($ch);

  $viewstate = '';
  $viewstategenerator = '';
  $eventvalidation = '';
  $usuario = 'servicall';
  $pass = 'servicall2021';

  $html = new DOMDocument;
  $html->loadHTML($respuesta);
  foreach ($html->getElementsByTagName('input') as $tag) {
    if($tag->getAttribute('name') === '__VIEWSTATE'){
      $viewstate = trim($tag->getAttribute("value"));
    }
    if($tag->getAttribute('name') === '__VIEWSTATEGENERATOR'){
      $viewstategenerator = trim($tag->getAttribute("value"));
    }
    if($tag->getAttribute('name') === '__EVENTVALIDATION'){
      $eventvalidation = trim($tag->getAttribute("value"));
    }
  }

  // echo $viewstate . "<br>";
  // echo $viewstategenerator . "<br>";
  // echo $eventvalidation . "<br>";

  /* ================== Parte 2 ===================== */

  $array = array(
    'ctl00$ScriptManager1' => 'ctl00$UpdatePanel1|ctl00$Validar',
    '__EVENTTARGET' => '',
    '__EVENTARGUMENT' => '',
    '__VIEWSTATE' => $viewstate,
    '__VIEWSTATEGENERATOR' => $viewstategenerator,
    '__EVENTVALIDATION' => $eventvalidation,
    'ctl00$TextoAlerta' => '',
    'ctl00$UserName' => $usuario,
    'ctl00$UserPass' => $pass,
    '__ASYNCPOST' => 'true',
    'ctl00$Validar' => ''
  );
  //
  // var_dump($array);

  $url2 = "https://gestion.ecertchile.cl/Default.aspx";

  $ch2 = curl_init();
  curl_setopt($ch2, CURLOPT_URL, $url2);
  curl_setopt($ch2, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0");
  curl_setopt($ch2, CURLOPT_HEADER, false);
  curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch2, CURLOPT_COOKIEFILE, $cookie);
  curl_setopt($ch2, CURLOPT_COOKIEJAR, $cookie);
  curl_setopt($ch2, CURLOPT_POST, true);
  curl_setopt($ch2, CURLOPT_POSTFIELDS, http_build_query($array));

  $respuesta2 = curl_exec($ch2);

  // echo $respuesta2;

  curl_close($ch2);

  $url = "https://gestion.ecertchile.cl/PKINueva/BuscarSol.aspx";

  $ch4 = curl_init();
  curl_setopt($ch4, CURLOPT_URL, $url);
  curl_setopt($ch4, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0");
  curl_setopt($ch4, CURLOPT_HEADER, false);
  curl_setopt($ch4, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch4, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch4, CURLOPT_COOKIEFILE, $cookie);
  curl_setopt($ch4, CURLOPT_COOKIEJAR, $cookie);
  curl_setopt($ch4, CURLOPT_POST, false);

  $respuesta4 = curl_exec($ch4);

  // echo $respuesta4;

  curl_close($ch4);

  $viewstate = '';
  $viewstategenerator = '';
  $eventvalidation = '';

  $html = new DOMDocument;
  $html->loadHTML($respuesta4);
  foreach ($html->getElementsByTagName('input') as $tag) {
    if($tag->getAttribute('name') === '__VIEWSTATE'){
      $viewstate = trim($tag->getAttribute("value"));
    }
    if($tag->getAttribute('name') === '__VIEWSTATEGENERATOR'){
      $viewstategenerator = trim($tag->getAttribute("value"));
    }
    if($tag->getAttribute('name') === '__EVENTVALIDATION'){
      $eventvalidation = trim($tag->getAttribute("value"));
    }
  }

  // echo $viewstate . "<br>";
  // echo $viewstategenerator . "<br>";
  // echo $eventvalidation . "<br>";

  /* ================== Parte 3 ===================== */

  $array = array(
    'ctl00$MainContent$ScriptManager1' =>	'ctl00$MainContent$UpdatePanel1|ctl00$MainContent$Buscar',
    '__EVENTTARGET' => '',
    '__EVENTARGUMENT' => '',
    '__VIEWSTATE' => $viewstate,
    '__VIEWSTATEGENERATOR' => $viewstategenerator,
    '__EVENTVALIDATION' => $eventvalidation,
    'ctl00$MainContent$FiltroEstadoSol' => '',
    'ctl00$MainContent$FiltroEstadoCert' => '',
    'ctl00$MainContent$FiltroPolitica' => '',
    'ctl00$MainContent$BuscarPor' => '1',
    'ctl00$MainContent$txt_NSolicitud' => '',
    'ctl00$MainContent$txt_Rut' => '12749393-6',
    'ctl00$MainContent$PaginaGrid' => '',
    'ctl00$MainContent$TotalRegistros' => '',
    'ctl00$MainContent$HiddenField1' => '0',
    'ctl00$MainContent$HiddenField2' => '',
    'ctl00$MainContent$ID_Politica' => '',
    'ctl00$MainContent$NSol_Select' => '',
    'ctl00$MainContent$Resultado' => '',
    '__ASYNCPOST' => 'true',
    'ctl00$MainContent$Buscar' => 'Buscar'
  );

  $url = "https://gestion.ecertchile.cl/PKINueva/BuscarSol.aspx";

  $ch5 = curl_init();
  curl_setopt($ch5, CURLOPT_URL, $url);
  curl_setopt($ch5, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0");
  curl_setopt($ch5, CURLOPT_HEADER, false);
  curl_setopt($ch5, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch5, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch5, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch5, CURLOPT_COOKIEFILE, $cookie);
  curl_setopt($ch5, CURLOPT_COOKIEJAR, $cookie);
  curl_setopt($ch5, CURLOPT_POST, true);
  curl_setopt($ch5, CURLOPT_POSTFIELDS, http_build_query($array));

  $respuesta5 = curl_exec($ch5);

  // echo $respuesta5;

  // $arch = fopen("solicitudes.html", "w+");
  // fwrite($arch, $respuesta5);
  // fclose($arch);

  $Numero = '';
  $Fecha = '';
  $Nombre = '';


  $html = new DOMDocument;
  $html->loadHTML($respuesta5);
  $z = 0;
  foreach ($html->getElementsByTagName('address') as $tag) {
    foreach ($tag->getElementsByTagName('strong') as $tag2) {
      $Nombre = trim($tag2->textContent);
    }
    $z++;
    if($z >= 0){
      break;
    }
  }
  foreach ($html->getElementsByTagName('table') as $tag) {
    if($tag->getAttribute('id') === 'GridView1'){
      foreach ($tag->getElementsByTagName('tbody') as $tag2) {
        $i = 0;
        foreach ($tag2->getElementsByTagName('tr') as $tag3) {
          $i++;
          $j = 0;
          foreach ($tag3->getElementsByTagName('td') as $tag4) {
            if($j == 1){
              $numero = trim($tag4->textContent);
            }
            if($j == 3){
              $fecha = date("d-m-Y", strtotime(trim($tag4->textContent)));
            }
            $j++;
          }
          if($i = 1){
            break;
          }
        }
      }
    }
  }

  echo $numero . "<br>";
  echo $fecha . "<br>";
  echo $Nombre . "<br>";

  curl_close($ch5);

  unlink($cookie);
?>
